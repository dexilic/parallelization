package rs.ninjava.parallelization;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ForeachExample {

	public static int getNumberOfWordsWithLength(List<String> words, int length) {

		int result = 0;

		for (String word : words) {
			if (word.length() == length) {
				result++;
			}
		}

		return result;
	}

	public static void sort(List<String> words) {

		// Collections.sort(words);

		for (String word : words) {
			char[] array = word.toCharArray();
			Arrays.sort(array);
			word = new String(array);
		}
	}

	public static boolean exists(List<String> words, String word) {

		for (String w : words) {
			if (w.equals(word)) {
				return true;
			}
		}

		return false;
	}

	public static Map<String, Integer> calculateLetterOccurrence(List<String> words) {

		Map<String, Integer> letterOccurrencesMap = new TreeMap<>();

		for (String word : words) {

			String[] letters = word.split("");

			for (int i = 0; i < letters.length; i++) {

				String letter = letters[i];

				if (letterOccurrencesMap.containsKey(letter)) {
					int numberOfOccurrences = letterOccurrencesMap.get(letter);
					letterOccurrencesMap.put(letter, ++numberOfOccurrences);
				} else {
					letterOccurrencesMap.put(letter, 1);
				}
			}
		}

		return letterOccurrencesMap;
	}
}
