package rs.ninjava.parallelization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 *
 */
public class ForkJoinExample {

	public static Map<String, Integer> calculateLetterOccurrence(List<String> words) {

		// Creates a ForkJoinPool with parallelism equal to java.lang.Runtime.availableProcessors.
		ForkJoinPool pool = new ForkJoinPool();

		LetterOccurrenceTask task = new LetterOccurrenceTask(words);

		pool.execute(task);

		Map<String, Integer> result = task.join();

		return result;
	}

	public static int getNumberOfWordsWithLength(List<String> words, int length) {

		System.out.println("Number of available processors: " + Runtime.getRuntime().availableProcessors());

		// Creates a ForkJoinPool with parallelism equal to java.lang.Runtime.availableProcessors.
		ForkJoinPool pool = new ForkJoinPool();

		NumberOfWordsWithLengthTask task = new NumberOfWordsWithLengthTask(words, length);

		pool.execute(task);

		Integer result = task.join();

		return result;
	}

	private static class LetterOccurrenceTask extends RecursiveTask<Map<String, Integer>> {

		private final List<String> words;

		public LetterOccurrenceTask(List<String> words) {
			this.words = words;
		}

		@Override
		protected Map<String, Integer> compute() {

			List<LetterOccurrenceTask> tasks = new ArrayList<>();

			// We are defining a threshold for parallel execution.
			if (words.size() > 10000) {

				// Divide list of words into two lists.
				int size = words.size();

				List<String> lowerList = words.subList(0, size / 2);
				List<String> upperList = words.subList(size / 2, size);

				LetterOccurrenceTask taskForLowerList = new LetterOccurrenceTask(lowerList);
				LetterOccurrenceTask taskForUpperList = new LetterOccurrenceTask(upperList);

				taskForLowerList.fork();
				taskForUpperList.fork();

				// Add tasks in reverse order, because it is more efficient to perform returns (joins) innermost-first.
				tasks.add(taskForUpperList);
				tasks.add(taskForLowerList);
			} else {

				// Below the threshold, we'll use base foreach method.
				return ForeachExample.calculateLetterOccurrence(words);
			}

			Map<String, Integer> resultMapFromTask = new HashMap<>();
			Map<String, Integer> finalResultMapFromAllTasks = new HashMap<>();

			for (LetterOccurrenceTask task : tasks) {
				resultMapFromTask = task.join();

				for (Entry<String, Integer> entry : resultMapFromTask.entrySet()) {

					final String key = entry.getKey();
					final Integer value = entry.getValue();

					if (finalResultMapFromAllTasks.containsKey(key)) {
						Integer numberOfOccurrences = finalResultMapFromAllTasks.get(key);
						numberOfOccurrences += value;
						finalResultMapFromAllTasks.put(key, numberOfOccurrences);
					} else {
						finalResultMapFromAllTasks.put(key, value);
					}
				}
			}

			return finalResultMapFromAllTasks;
		}
	}

	private static class NumberOfWordsWithLengthTask extends RecursiveTask<Integer> {

		private final List<String> words;
		private final int length;

		public NumberOfWordsWithLengthTask(List<String> words, int length) {
			this.words = words;
			this.length = length;
		}

		@Override
		protected Integer compute() {

			Integer result = 0;

			List<NumberOfWordsWithLengthTask> tasks = new ArrayList<>();

			if (words.size() > 100) {

				// Divide list of words into two lists.
				int size = words.size();

				List<String> lowerList = words.subList(0, size / 2);
				List<String> upperList = words.subList(size / 2, size);

				NumberOfWordsWithLengthTask taskForLowerList = new NumberOfWordsWithLengthTask(lowerList, length);
				NumberOfWordsWithLengthTask taskForUpperList = new NumberOfWordsWithLengthTask(upperList, length);

				taskForLowerList.fork();
				taskForUpperList.fork();

				tasks.add(taskForLowerList);
				tasks.add(taskForUpperList);
			} else {
				return Integer.valueOf(ForeachExample.getNumberOfWordsWithLength(words, length));
			}

			for (NumberOfWordsWithLengthTask task : tasks) {
				result += task.join();
			}

			return result;
		}
	}
}
