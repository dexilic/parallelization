package rs.ninjava.parallelization;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceExample {

	public static int getNumberOfWordsWithLength(List<String> words, int length, String word, int nThreads) {

		long startTime = System.currentTimeMillis();

		ExecutorService pool = Executors.newFixedThreadPool(nThreads);

		long duration = System.currentTimeMillis() - startTime;

		System.out.println("Instatiating fixed thread pool of " + nThreads + " threads took " + duration + " ms.");

		startTime = System.currentTimeMillis();

		// List<Future<Integer>> futures = new ArrayList<>();
		List<Future<Boolean>> futures = new ArrayList<>();
		List<Future<?>> runnables = new ArrayList<>();

		// Divide list into as many sublists as threads
		List<List<String>> subLists = divideList(words, nThreads);

		duration = System.currentTimeMillis() - startTime;

		System.out.println("Dividing a list into " + nThreads + " sublists took " + duration + " ms.");

		startTime = System.currentTimeMillis();

		for (int i = 0; i < nThreads; i++) {

			List<String> subList = subLists.get(i);

			// Future<Integer> future = pool.submit(new NumberOfWordsWithLengthCallable(subList, length));
			// Future<Boolean> future = pool.submit(new WordExistsCallable(subList, word));
			//
			// futures.add(future);

			Future<?> runnable = pool.submit(new SortWordsRunnable(subList));
			runnables.add(runnable);
		}

		duration = System.currentTimeMillis() - startTime;

		System.out.println("Submitting " + nThreads + " tasks took " + duration + " ms.");

		startTime = System.currentTimeMillis();

		// int result = 0;
		Boolean result = true;
		try {
			// Get results from all future tasks
			for (int i = 0; i < nThreads; i++) {

				// Future<Integer> future = futures.get(i);
				// Future<Boolean> future = futures.get(i);

				// result &= future.get();

				Future<?> runnable = runnables.get(i);
				runnable.get();
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		duration = System.currentTimeMillis() - startTime;

		System.out.println("Obtaining results from " + nThreads + " tasks took " + duration + " ms.");

		// return result;
		return result ? 1 : 0;
	}

	private static List<List<String>> divideList(List<String> words, int numberOfSubLists) {

		List<List<String>> subLists = new ArrayList<>();

		int subListSize = words.size() / numberOfSubLists;

		for (int i = 0; i < numberOfSubLists; i++) {

			List<String> subList = words.subList(subListSize * i, subListSize * (i + 1));

			subLists.add(subList);
		}

		return subLists;
	}

	public static Map<String, Integer> calculateLetterOccurrences(List<String> words, int nThreads) {

		/*
		 * Creating new fixed thread pool.
		 */
		long startTime = System.currentTimeMillis();

		ExecutorService pool = Executors.newFixedThreadPool(nThreads);

		long duration = System.currentTimeMillis() - startTime;

		System.out.println("Instatiating fixed thread pool of " + nThreads + " threads took " + duration + " ms.");

		/*
		 * Dividing a list into as many sublists as there are threads.
		 */
		startTime = System.currentTimeMillis();

		List<Future<Map<String, Integer>>> futures = new ArrayList<>();

		List<List<String>> subLists = divideList(words, nThreads);

		duration = System.currentTimeMillis() - startTime;

		System.out.println("Dividing a list into " + nThreads + " sublists took " + duration + " ms.");

		/*
		 * Submitting work to threads.
		 */
		startTime = System.currentTimeMillis();

		for (int i = 0; i < nThreads; i++) {

			List<String> subList = subLists.get(i);

			Future<Map<String, Integer>> future = pool.submit(new LetterOccurrenceCallable(subList));

			futures.add(future);
		}

		duration = System.currentTimeMillis() - startTime;

		System.out.println("Submitting " + nThreads + " tasks took " + duration + " ms.");

		/*
		 * Obtain results from all future tasks.
		 */
		startTime = System.currentTimeMillis();

		Map<String, Integer> resultMap = new TreeMap<>();

		try {
			for (int i = 0; i < nThreads; i++) {

				Future<Map<String, Integer>> future = futures.get(i);

				Map<String, Integer> resultMapFromThread = future.get();

				for (Entry<String, Integer> entry : resultMapFromThread.entrySet()) {

					final String key = entry.getKey();
					final Integer value = entry.getValue();

					if (resultMap.containsKey(key)) {
						Integer numberOfOccurrences = resultMap.get(key);
						numberOfOccurrences += value;
						resultMap.put(key, numberOfOccurrences);
					} else {
						resultMap.put(key, value);
					}
				}
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		duration = System.currentTimeMillis() - startTime;

		System.out.println("Obtaining results from " + nThreads + " tasks took " + duration + " ms.");

		return resultMap;
	}

	private static class NumberOfWordsWithLengthCallable implements Callable<Integer> {

		private final List<String> words;
		private final int length;

		public NumberOfWordsWithLengthCallable(List<String> words, int length) {
			this.words = words;
			this.length = length;
		}

		@Override
		public Integer call() {
			return Integer.valueOf(ForeachExample.getNumberOfWordsWithLength(words, length));
		}
	}

	private static class SortWordsRunnable implements Runnable {

		private final List<String> words;

		public SortWordsRunnable(List<String> words) {
			this.words = words;
		}

		@Override
		public void run() {
			ForeachExample.sort(words);
		}
	}

	private static class WordExistsCallable implements Callable<Boolean> {

		private final List<String> words;
		private final String word;

		public WordExistsCallable(List<String> words, String word) {
			this.words = words;
			this.word = word;
		}

		@Override
		public Boolean call() throws Exception {
			return ForeachExample.exists(words, word);
		}
	}

	private static class LetterOccurrenceCallable implements Callable<Map<String, Integer>> {

		private final List<String> words;

		public LetterOccurrenceCallable(List<String> words) {
			this.words = words;
		}

		@Override
		public Map<String, Integer> call() throws Exception {
			return ForeachExample.calculateLetterOccurrence(words);
		}
	}
}
