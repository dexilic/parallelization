package rs.ninjava.parallelization;

import java.util.List;
import java.util.Map;

import rs.ninjava.util.TestFileGenerator;
import rs.ninjava.util.TestFileReader;

/**
 * Main class for starting a program.
 */
public class Parallelization {

	private static final int NUMBER_OF_THREADS = 4;
	private static final int WORD_LENGTH = 16;
	private static final String WORD = "fkrxucuoxplzwzwvmx";

	public static void main(String[] args) {

		System.out.println("Number of available processors: " + Runtime.getRuntime().availableProcessors());
		System.out.println("Reading a file with random words...");

		/*
		 * Loading a list of words from a file.
		 */

		long startTime = System.currentTimeMillis();

		List<String> words = TestFileReader.getWords(TestFileGenerator.RANDOM_WORDS_FILE_NAME);

		long duration = System.currentTimeMillis() - startTime;

		String message = String.format("Reading a file with %d words took %d ms.",
				TestFileGenerator.MAX_NUMBER_OF_WORDS, duration);
		System.out.println(message);

		/*
		 * Obtaining numbers of each letter's occurrence in the list using simple foreach approach.
		 */
		System.out.println("Starting foreach example...");

		startTime = System.currentTimeMillis();

		Map<String, Integer> letterOccurencesMapFromForeachExample = ForeachExample.calculateLetterOccurrence(words);

		duration = System.currentTimeMillis() - startTime;

		message = String.format("Foreach example, took %d ms.", duration);
		System.out.println(message);

		/*
		 * Obtaining numbers of each letter's occurrence in the list using ExecutorService approach.
		 */
		System.out.println("Starting ExecutorService example...");

		startTime = System.currentTimeMillis();

		Map<String, Integer> letterOccurencesMapFromExecutorServiceExample = ExecutorServiceExample
				.calculateLetterOccurrences(words, NUMBER_OF_THREADS);

		duration = System.currentTimeMillis() - startTime;

		message = String.format("ExecutorService example, took %d ms.", duration);
		System.out.println(message);

		/*
		 * Obtaining numbers of each letter's occurrence in the list using Fork/Join framework.
		 */
		System.out.println("Starting ForkJoin example...");

		startTime = System.currentTimeMillis();

		Map<String, Integer> letterOccurencesMapFromForkJoinExample = ForkJoinExample.calculateLetterOccurrence(words);

		duration = System.currentTimeMillis() - startTime;

		message = String.format("ForkJoin example, took %d ms.", duration);
		System.out.println(message);

		// System.out.println("Foreach example, size of the map: " + letterOccurencesMapFromForeachExample.size());
		// System.out.println("ExecutorService example, size of the map: "
		// + letterOccurencesMapFromExecutorServiceExample.size());
		// System.out.println("ForkJoin example, size of the map: " + letterOccurencesMapFromForkJoinExample.size());
		//
		// for (Entry<String, Integer> entry : letterOccurencesMapFromForeachExample.entrySet()) {
		// final String key = entry.getKey();
		// System.out.println(key + "-" + entry.getValue() + "///"
		// + letterOccurencesMapFromExecutorServiceExample.get(key) + "///"
		// + letterOccurencesMapFromForkJoinExample.get(key));
		// }
	}
}
