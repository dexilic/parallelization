package rs.ninjava.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for generating test file with random words.
 */
public class TestFileGenerator {

	public static final String RANDOM_WORDS_FILE_NAME = "RandomWords.txt";
	public static final int MAX_NUMBER_OF_WORDS = 1_000_000;

	private static final byte RANDOM_WORD_MAX_LENGTH = 20;

	public static void main(String[] args) {

		List<String> words = fillListWithRandomWords();

		flushListToFile(words);
	}

	private static List<String> fillListWithRandomWords() {

		List<String> words = new ArrayList<>();

		long startTime = System.currentTimeMillis();

		for (int i = 0; i < MAX_NUMBER_OF_WORDS; i++) {
			final String randomWord = RandomWordGenerator.getWord(RANDOM_WORD_MAX_LENGTH);
			words.add(randomWord);
		}

		long endTime = System.currentTimeMillis() - startTime;

		System.out.println("Generating a list of " + MAX_NUMBER_OF_WORDS + " random words took " + endTime + " ms.");

		return words;
	}

	private static void flushListToFile(List<String> words) {

		long startTime = System.currentTimeMillis();

		ClassLoader classLoader = TestFileGenerator.class.getClassLoader();
		File file = new File(classLoader.getResource(RANDOM_WORDS_FILE_NAME).getFile());

		try (FileWriter fw = new FileWriter(file)) {
			for (String word : words) {
				fw.write(word + "\n");
			}
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		long endTime = System.currentTimeMillis() - startTime;

		System.out.println("Writing a list of " + MAX_NUMBER_OF_WORDS + " words to a file took " + endTime + " ms.");
	}
}
