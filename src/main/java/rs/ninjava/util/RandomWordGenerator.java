package rs.ninjava.util;

import java.util.Random;

public class RandomWordGenerator {

	private static final int NUMBER_OF_LETTERS_IN_ENGLISH_ALPHABET = 26;

	private static Random random = new Random();

	/**
	 * Gets random word with length not greater than a given parameter.
	 * 
	 * @param maxLength
	 *            maximum length of a generated random word
	 * @return random word
	 */
	public static String getWord(byte maxLength) {

		int numberOfLetters = random.nextInt(maxLength) + 1;

		char[] word = new char[numberOfLetters];

		// Generate a random word
		for (int j = 0; j < numberOfLetters; j++) {
			word[j] = (char) ('a' + random.nextInt(NUMBER_OF_LETTERS_IN_ENGLISH_ALPHABET));
		}

		return new String(word);
	}
}
