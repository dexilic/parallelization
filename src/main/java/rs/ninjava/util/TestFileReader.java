package rs.ninjava.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Utility class for reading test file with random words.
 */
public class TestFileReader {

	public static List<String> getWords(String fileName) {

		List<String> words = new ArrayList<>();

		ClassLoader classLoader = TestFileReader.class.getClassLoader();
		File file = new File(classLoader.getResource(TestFileGenerator.RANDOM_WORDS_FILE_NAME).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				words.add(line);
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return words;
	}
}
