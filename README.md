# README #

# Quick summary
Purpose of this Java project is to explore various ways how can we parallelize data processing in Java application.

## Description
Data being processed represents large number of randomly generated words contained in a file. Our task is to process all these words to establish how many times each letter occurs. This way we can tell how random is our word generator, i.e. does it really create uniform distribution of alphabet letters. If so, then numbers of each letter's occurrence should be more or less equal. 
This is what we want to establish, and in the same time, to explore various ways how can we parallelize our work. 

### Techniques used in our example:
* Basic foreach loop which doesn't use parallelization, and is executed in a single thread,
* ExecutorService,
* ForkJoin,
* Stream parallelization, and 
* Akka as a Java parallelization framework.

### How do I get set up? ###

* Download code from this BitBucket repository.
* Run mvn clean install -U
* Run Java application (main class is Parallelization.java)